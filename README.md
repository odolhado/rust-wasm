
Install: 

    curl https://sh.rustup.rs -sSf | sh         # installs rust for Linux
    brew install rustup                         # installs rust for OSX

    rustup-init
    rustup default nightly                      # beacuse nightly version supports webassembly
    rustup target add wasm32-unknown-unknown    # could be : ... add wasm32-x68_64-apple
    
    
    cargo install wasm-pack                     # Cargo is rust package manager - updates crates.io index 
    cargo install wasm-gc                       # tool to remove all unneeeded exports imports functions, from webasembly module
    
    cargo install https                        # downloads http binary, runs web server
    
    
Start the project

    wasm-pack build         # run at /utils/
    npx webpack-dev-server  # run at /utils/
    
Development

    cargo new --lib utils                       # creates utils/library project
    
    cargo build --target wasm32-unknown-unknown --release
    
    wasm-gc target/wasm32-unknown-unknown/release/utils.wasm
    wasm-gc target/wasm32-unknown-unknown/release/utils.wasm -o utils.gc.wasm
    
Start server
    
    http
    
Open browser

    localhost:8000 // rust server
    localhost:8080 // webpack server
     
    
Additional packages

    cargo install wasm-bindgen
    wasm-pack build
    
    npm install -D webpack webpack-cli webpack-dev-server
    
    
